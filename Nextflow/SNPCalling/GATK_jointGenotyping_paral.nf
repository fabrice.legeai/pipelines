#!/usr/bin/env nextflow

/*
 * Defines the pipeline inputs parameters (giving a default value for each for them)
 * Each of the following parameters can be specified as command line options
 */

/* genome creation */
params.genome = "$baseDir/samples/genome.fa"
genomeFasta = file(params.genome)
params.bwa_index = false

/* results */
params.resultsDir = "./GATK_gVCF_results2/"

/* executor */
params.executor = "slurm"
params.queue = "genouest"

results = file(params.resultsDir)

/* parameters */
params.cohort = "test"
params.intervals = "$baseDir/samples/intervals.list"
params.samples = "$baseDir/samples/samples.csv"
gvcfs = file(params.samples)
params.het_mean=0.001
params.het_sd=0.01
params.ploidy=2

/* parameters for all */
params.nbthreads = 12
params.mem = 20

intervals_ch = Channel
    .fromPath(params.intervals)
    .splitCsv(sep:"\t")
    .map{ row-> tuple(row[0],row[1], row[2])}

process index {
    executor params.executor
    queue params.queue

    storeDir "."

    input:
    file genomeFasta from genomeFasta

    output:
    file(index) into genome_index

    script:
    """
    mkdir index
    cp  ${params.genome} index/genome.fa
    bwa index -p index/genome.index index/genome.fa
    samtools faidx index/genome.fa
    picard CreateSequenceDictionary R=index/genome.fa O=index/genome.dict
    """
}

process GenomicsDBImport {
    executor params.executor
    queue params.queue

    input:
    file (gvcfs) from gvcfs
    set scaffold, begin,end from intervals_ch

    output:
    set scaffold, "genomicsDB.${scaffold}" into gendb_ch

//    publishDir "${results}/", mode: 'copy'

    script:
    """
    gatk GenomicsDBImport --java-options "-Xmx${params.mem}g -Xms${params.mem}g" \
    --sample-name-map ${gvcfs} \
    --intervals ${scaffold} \
    --genomicsdb-workspace-path genomicsDB.${scaffold}
    """
}

/* Genoptyping */

process GenotypeGVCFs {
    executor params.executor
    queue params.queue
    publishDir "${results}/", mode: 'copy'

    //publishDir params.output_dir, mode: 'copy', pattern: '*.{vcf,idx}'

    input:
	set interval, file (workspace) from gendb_ch
        file(index) from genome_index

    output:
    file("${params.cohort}.${interval}.vcf") into (vcf_ch)
    file("${params.cohort}.${interval}.vcf.idx") into (vcf_idx_ch)

    script:
    """
    gatk --java-options "-Xmx${params.mem}g -Xms${params.mem}g" \
     GenotypeGVCFs \
     -R index/genome.fa \
     -O ${params.cohort}.${interval}.vcf \
     -V gendb://${workspace} \
     --heterozygosity ${params.het_mean} \
     --heterozygosity-stdev ${params.het_sd} \
     --sample-ploidy ${params.ploidy} \
     """
}

process GatherVcfs {
    executor params.executor
    queue params.queue
    publishDir "${results}/", mode: 'copy'
    input:
    file (vcf) from vcf_ch.collect()
    file (vcf_idx) from vcf_idx_ch.collect()

    output:
    set file("${params.cohort}.vcf"), file("${params.cohort}.vcf.idx") into (end_ch)

    // WARNING : complicated channel extraction! 
    // GATK GatherVcfs only accepts as input VCF in the chromosomical order. Nextflow/Groovy list are not sorted. The following command does :
    // 1 : look for all VCF with "chr[0-9]*" in the filename (\d+ means 1 or + digits)
    // 2 : Tokenize the filenames with "." as the separator, keep the 2nd item (indexed [1]) "chr[0-9]*"
    // 3 : Take from the 3rd character till the end of the string "chr[0-9]*", ie the chromosome number
    // 4 : Cast it from a string to an integer (to force a numerical sort)
    // 5 : Sort 
    // 6 : Add chrX and chrY to the list

    script:
    """
    gatk --java-options "-Xmx${params.mem}g -Xms${params.mem}g" \
      GatherVcfs \
      ${vcf.findAll{ it=~/scaffold\d+/ }.collect().sort{ it.name.tokenize('.')[1].substring(8).toInteger() }.collect{ "--INPUT $it " }.join() } \
      --OUTPUT ${params.cohort}.vcf
    """
}	




workflow.onComplete {
	def subject = "[PoolSeq-Map] Successful: $workflow.runName"
	if(!workflow.success){
		subject = "[PoolSeq-Map] FAILED: $workflow.runName"
	}

	def msg = """
	${subject}
	---------------------------
	Pipeline execution summary
	---------------------------
	Completed at: ${workflow.complete}
	Duration    : ${workflow.duration}
	Success     : ${workflow.success}
	workDir     : ${workflow.workDir}
	exit status : ${workflow.exitStatus}
	"""
	.stripIndent()

	println msg
}
