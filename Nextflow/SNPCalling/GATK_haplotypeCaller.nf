#!/usr/bin/env nextflow

/*
 * Defines the pipeline inputs parameters (giving a default value for each for them)
 * Each of the following parameters can be specified as command line options
 */

/* genome creation */
params.genome = "$baseDir/samples/genome.fa"
genomeFasta = file(params.genome)
params.bwa_index = false

/* results */
params.resultsDir = "./GATK_HC_results/"

/* executor */
params.executor = "slurm"
params.queue = "genouest"

results = file(params.resultsDir)


/* parameters */
params.het_mean=0.001
params.het_sd=0.01
params.ploidy=2

/* parameters for all */
params.nbthreads = 12
params.mem = 20


params.samples = "$baseDir/samples/samples.csv"

Channel
    .fromPath(params.samples)
    .splitCsv(header:true)
    .map{ row-> tuple(row.sampleId, row.sampleName, file(row.read1), file(row.read2)) }
    .set { samples_ch }

process fastp {

    executor params.executor
    queue params.queue

    publishDir "${results}/${sampleId}", mode: 'copy'

    input:
    set sampleId, sampleName, file(read1), file(read2) from samples_ch

    output:
    file '*.fastp.{json,html}' into fastp_result
    set sampleId, sampleName, "${sampleId}.clean_R1.fq.gz", "${sampleId}.clean_R2.fq.gz"  into clean_files

    script:
    """
    fastp  -i $read1 -I $read2 -o ${sampleId}.clean_R1.fq.gz -O ${sampleId}.clean_R2.fq.gz -w ${params.nbthreads} -h ${sampleId}.fastp.html -j ${sampleId}.fastp.json
    """
}



process index {
    executor params.executor
    queue params.queue
    
    storeDir "."

    input:
    file genomeFasta from genomeFasta
	  
    output:
    file(index) into genome_index
	  
    script:
    """
    mkdir index
    cp ${genomeFasta} index/genome.fa
    bwa index -p index/genome.index index/genome.fa
    samtools faidx index/genome.fa
    picard CreateSequenceDictionary R=index/genome.fa O=index/genome.dict
    """
}

process map {
    executor params.executor
    queue params.queue

    input:
    set sampleId, sampleName, file(clean_R1),file(clean_R2)  from  clean_files
    file(bwa_index) from genome_index

    output:
    set sampleId, "${sampleId}.bam", "${sampleId}.bam.bai" into mapping,stats_fromMap
 
    script:
    """
    bwa mem -M -t ${params.nbthreads} -R \"@RG\\tID:${sampleName}\\tPL:ILLUMINA\\tLB:LIB-${sampleId}\\tSM:${sampleName}\" ${bwa_index}/genome.index $clean_R1 $clean_R2  | samtools sort -T ${sampleId} -@ ${params.nbthreads} -O bam -o ${sampleId}.bam
    samtools index ${sampleId}.bam
    """
}

process markdup {
  executor params.executor
  queue params.queue

  publishDir "${results}/mapping", mode: 'copy'

  input:
  set name, file(bam), file(bai) from mapping

  output:
  set name, "${name}_MarkDup.bam", "${name}_MarkDup.bam.bai"  into md
  file "MD_metrics.txt" into markdup_multiqc

  """
  gatk MarkDuplicates -I $bam -M MD_metrics.txt -O ${name}_MarkDup.bam
  samtools index ${name}_MarkDup.bam
  """

}

process haplotypecaller {
   executor params.executor
   queue params.queue
  
   publishDir "${results}/VCF", mode: 'copy'
   
    input:
    file(index) from genome_index
    set sampleID, file(bam),file(bai) from md
	
    output:
    set sampleID, "${sampleID}.g.vcf" , "${sampleID}.g.vcf.idx" into gvcf_ch
	
    script:
    """
    gatk  --java-options "-Xmx${params.mem}g -Xms${params.mem}g" \
		HaplotypeCaller \
		-R index/genome.fa \
		-I ${bam} \
		-O ${sampleID}.g.vcf \
		--heterozygosity ${params.het_mean} \
		--heterozygosity-stdev ${params.het_sd} \
		--sample-ploidy ${params.ploidy} \
		-ERC GVCF		
    """
}	

process stats {
    executor params.executor
   queue params.queue

    publishDir "${results}/mapping", mode: 'copy'

    input:
    set sampleId, file(bam_fromMap),file (bai) from stats_fromMap

    output:
    file "${bam_fromMap}.stats" 

    script:
    """
    samtools stats -@ ${params.nbthreads} $bam_fromMap > ${bam_fromMap}.stats
    """
}


workflow.onComplete {
	def subject = "[PoolSeq-Map] Successful: $workflow.runName"
	if(!workflow.success){
		subject = "[PoolSeq-Map] FAILED: $workflow.runName"
	}

	def msg = """
	${subject}
	---------------------------
	Pipeline execution summary
	---------------------------
	Completed at: ${workflow.complete}
	Duration    : ${workflow.duration}
	Success     : ${workflow.success}
	workDir     : ${workflow.workDir}
	exit status : ${workflow.exitStatus}
	"""
	.stripIndent()

	println msg
}
