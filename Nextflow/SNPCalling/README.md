the Nextflow pipelines deposited here are :

# GATK_haplotypeCaller :

It performs a basic SNPcalling using the GATK4 haplotypeCaller sotfware on each DNAseq samples 
  
This pipeline runs the following steps :

1. *fastp* for the cleaning of the reads
2. *bwa mem* for the mapping of the reads
3. *gatk markduplicates* for the PCR duplications removal

Parameters :

* genome  : fasta file of the genome (mandatory)
* samples : a comma separated text file  (with an header) describing the fastq files and samples (mandatory)

example of the content of a samples file :
```
sampleId,sampleName,read1,read2
A_test,A,mydata/A_R1.fastq.gz,mydata/Alfalfa_GL349621_R2.fastq.gz
B_test,B,mydata/B_R1.fastq.gz,mydata/Alfalfa_GL349621_R2.fastq.gz
```

* het_mean : the average heterozygoty used by haplotypeCaller (def = 0.001)
* het_sd : the standard deviation of  heterozygoty used by haplotypeCaller (def = 0.01)
* ploidy : the ploidy used by haplotypeCaller (def =2)
* resultsDir :  the directory where the individual genotypes i gVCF format will be stored (def = ./GATK_HC_results/)
* executor  : the excution modei, see Nextflow documentation  (default = slurm")
* queue : the queue if needed (default = genouest)
* nbthreads : numbers of threads to use (def  = 12)
* mem : max memory to use (def =  20)

# GATK_jointGenotyping_paral.nf

This pipeline take the gVCF (output of haplotypeCaller) and merge the into a unique VCF file in 3 steps (GenomicsDBImport, GenotypeGVCFs, and GatherVcfs). It runs in parallel on each interval

Parameters are :

* genome  : fasta file of the genome (mandatory)
* samples : a tabular text file listing the samples and corresponding  gVCF files  (mandatory)

example :
```
A gVCF/A.g.vcf
B gVCF/B.g.vcf
```

* intervals : the list of the scaffold to run (with start and end positions) as follow : 

scaffold1	1 1000000
scaffold2	1 700000

* cohort : the name of the final VCF file
* het_mean : the average heterozygoty used by haplotypeCaller (def = 0.001)
* het_sd : the standard deviation of  heterozygoty used by haplotypeCaller (def = 0.01)
* ploidy : the ploidy used by haplotypeCaller (def =2)
* resultsDir :  the directory where the individual genotypes i gVCF format will be stored (def = ./GATK_HC_results/)
* executor  : the excution modei, see Nextflow documentation  (default = slurm")
* queue : the queue if needed (default = genouest)
* nbthreads : numbers of threads to use (def  = 12)
* mem : max memory to use (def =  20)

